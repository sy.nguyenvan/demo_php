<?php
include 'templates/header.php';

function exclaim($str) {
    return $str . "! ";
}

function ask($str) {
    return $str . "? ";
}

function printFormatted($str, $format) {
    // Calling the $format callback function
    echo $format($str),"<br>";
}

// Pass "exclaim" and "ask" as callback functions to printFormatted()
printFormatted("Hello world", "exclaim");
printFormatted("Hello world", "ask");



?>

<!--Code HTML-->
<div>
    <h1>Helloooooooooo</h1>
</div>

<?php include 'templates/footer.php'; ?>

